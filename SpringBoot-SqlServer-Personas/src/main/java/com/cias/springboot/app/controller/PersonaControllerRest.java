package com.cias.springboot.app.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cias.springboot.app.model.Persona;
import com.cias.springboot.app.model.dao.InterfacePersonaRestDao;

@RestController
@RequestMapping("/rest")
public class PersonaControllerRest {
	
	@Autowired
	private InterfacePersonaRestDao personaRest;
	
	@GetMapping("/personas")
	public List<Persona> listarPersonas(){
		return personaRest.findAll();
	}
	
	@GetMapping("/personas/{id}")
	public ResponseEntity<Persona> consultarPersona(@PathVariable(value = "id") Integer id) 
			throws ResourceNotFoundException{
		Persona persona = personaRest.findById(id).orElseThrow(() -> new 
				ResourceNotFoundException("La persona con Id "+ id + " no existe"));
		return ResponseEntity.ok().body(persona);
	}
	
	@PostMapping("/personas")
	public Persona crearPersona (@RequestBody Persona persona) {
		return personaRest.save(persona);
	}
	
	@PutMapping("/personas/{id}")
	public ResponseEntity<Persona> actualizarPErsona(@PathVariable(value = "id") Integer id, 
			                                         @RequestBody Persona persona_upd)
			throws ResourceNotFoundException{
		
		Persona persona = personaRest.findById(id).orElseThrow(() -> new 
				ResourceNotFoundException("La persona con Id "+ id + " no existe"));
		
		persona.setNombre(persona_upd.getNombre());
		persona.setPrimerApellido(persona_upd.getPrimerApellido());
		persona.setSegundoApellido(persona_upd.getSegundoApellido());
		persona.setTelefono(persona_upd.getTelefono());
		persona.setEstatus(persona_upd.getEstatus());
		
		return ResponseEntity.ok(personaRest.save(persona));
	}
	
	  @DeleteMapping("/personas/{id}")
	    public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") Integer id)
	         throws ResourceNotFoundException {
	        Persona persona = personaRest.findById(id)
	       .orElseThrow(() -> new ResourceNotFoundException("La persona con Id "+ id + " no existe"));

	        personaRest.delete(persona);
	        Map<String, Boolean> response = new HashMap<>();
	        response.put("deleted", Boolean.TRUE);
	        return response;
	    }
	
}
